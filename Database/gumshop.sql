-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 23, 2022 at 09:45 AM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gumshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `Category_ID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Category_Name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Category_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`Category_ID`, `Category_Name`) VALUES
('TL1', 'Gum ngậm'),
('TL2', 'Gum nhai'),
('TL3', 'Gum ád'),
('TL4', 'Gum df'),
('TL5', 'Gum hjk'),
('TL6', 'Gum u'),
('TL7', 'Gum re'),
('TL8', 'Gum ngậm'),
('TL9', 'Gum dfg');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `Cus_ID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Cus_Name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Phone_Number` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Cus_Address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Acc_username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Acc_password` text COLLATE utf8_unicode_ci NOT NULL,
  `avatar` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Cus_ID`),
  UNIQUE KEY `Cus_username` (`Acc_username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`Cus_ID`, `Cus_Name`, `Phone_Number`, `Cus_Address`, `Acc_username`, `Acc_password`, `avatar`) VALUES
('', 'd', 'ssdfdsf', '', '', 'd41d8cd98f00b204e9800998ecf8427e', ''),
('admin', 'Xim', '0348304341', 'Hà Tĩnh', 'admin', 'c4ca4238a0b923820dcc509a6f75849b', 'user-avatar.png'),
('KH1', 'Khách hàng 1', '0123456789', 'Hà Nội', 'kh1', 'c4ca4238a0b923820dcc509a6f75849b', 'avatar-mini-3.jpg'),
('KH10', 'Khách hàng 10', '1234567890', 'tp. Hồ Chí Minh', 'kh10', 'c4ca4238a0b923820dcc509a6f75849b', 'avatar-mini-3.jpg'),
('KH2', 'Khách hàng 2', '1234567890', 'tp. Hồ Chí Minh', 'kh2', 'c4ca4238a0b923820dcc509a6f75849b', 'avatar-mini-3.jpg'),
('KH3', 'Khách hàng 3', '0123456789', 'Hà Nội', 'kh3', 'c4ca4238a0b923820dcc509a6f75849b', 'avatar-mini-3.jpg'),
('KH4', 'Khách hàng 4', '1234567890', 'tp. Hồ Chí Minh', 'kh4', 'c4ca4238a0b923820dcc509a6f75849b', 'avatar-mini-3.jpg'),
('KH5', 'Khách hàng 5', '0123456789', 'Hà Nội', 'kh5', 'c4ca4238a0b923820dcc509a6f75849b', 'avatar-mini-3.jpg'),
('KH6', 'Khách hàng 6', '1234567890', 'tp. Hồ Chí Minh', 'kh6', 'c4ca4238a0b923820dcc509a6f75849b', 'avatar-mini-3.jpg'),
('KH7', 'Khách hàng 7', '0123456789', 'Hà Nội', 'kh7', 'c4ca4238a0b923820dcc509a6f75849b', 'avatar-mini-3.jpg'),
('KH8', 'Khách hàng 8', '1234567890', 'tp. Hồ Chí Minh', 'kh8', 'c4ca4238a0b923820dcc509a6f75849b', 'avatar-mini-3.jpg'),
('KH9', 'Khách hàng 9', '0123456789', 'Hà Nội', 'kh9', 'c4ca4238a0b923820dcc509a6f75849b', 'avatar-mini-3.jpg'),
('nvb', 'Nguyễn Văn B', '3241231232', 'Hà Nam', 'nvb', 'c4ca4238a0b923820dcc509a6f75849b', 'images (1).png');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `Order_ID` int(10) NOT NULL AUTO_INCREMENT,
  `Cus_ID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Order_Date` datetime NOT NULL,
  `total_price` float NOT NULL,
  `Shipper_ID` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` bit(1) NOT NULL,
  PRIMARY KEY (`Order_ID`) USING BTREE,
  KEY `Cus_ID` (`Cus_ID`) USING BTREE,
  KEY `orders_ibfk_3` (`Shipper_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=411 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`Order_ID`, `Cus_ID`, `Order_Date`, `total_price`, `Shipper_ID`, `Status`) VALUES
(1, 'KH9', '2021-01-28 09:22:36', 440000, 'Ship1', b'1'),
(2, 'KH1', '2021-01-20 00:00:00', 22000, 'Ship1', b'1'),
(4, 'KH5', '2021-01-20 00:00:00', 22000, 'Ship1', b'1'),
(5, 'KH6', '2021-01-08 00:00:00', 100000, 'Ship5', b'0'),
(7, 'KH8', '2021-01-05 00:00:00', 100000, 'Ship5', b'0'),
(8, 'KH3', '2021-01-28 00:00:00', 22000, 'Ship6', b'1'),
(9, 'KH8', '2021-01-05 00:00:00', 22000, 'Ship5', b'1'),
(10, 'KH3', '2021-01-21 00:00:00', 22000, 'Ship6', b'0'),
(11, 'KH9', '2021-01-19 00:00:00', 22000, 'Ship4', b'1'),
(298, 'KH1', '2021-01-27 07:50:51', 120000, NULL, b'1'),
(299, 'KH1', '2021-01-27 08:40:33', 219000, NULL, b'1'),
(378, 'KH1', '2021-01-28 00:33:16', 8097000, NULL, b'1'),
(379, 'KH2', '2021-01-27 10:40:35', 115000, NULL, b'1'),
(380, 'KH2', '2022-01-08 00:32:50', 463000, NULL, b'1'),
(398, 'nvb', '2021-01-27 19:49:37', 0, NULL, b'1'),
(399, 'nvb', '2021-01-27 19:54:44', 0, NULL, b'1'),
(400, 'nvb', '2021-01-27 19:56:08', 120000, NULL, b'0'),
(401, 'admin', '2021-01-27 22:58:20', 120000, NULL, b'0'),
(402, 'KH1', '2022-01-08 00:37:10', 2613120, NULL, b'1'),
(403, 'KH7', '2021-01-28 00:35:57', 1440000, NULL, b'0'),
(404, 'KH9', '2021-01-28 09:23:26', 186480000, NULL, b'0'),
(405, 'KH2', '2022-01-08 00:33:34', 12590000, NULL, b'1'),
(406, 'KH2', '2022-01-08 00:33:45', 59000, NULL, b'0'),
(407, 'KH1', '2022-01-08 00:43:14', 476000, NULL, b'1'),
(408, 'KH1', '2022-01-10 14:12:38', 19446000, NULL, b'1'),
(409, 'KH1', '2022-02-21 08:17:04', 1003000, NULL, b'1'),
(410, 'KH1', '2022-02-21 08:17:54', 59000, NULL, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `orders_detail`
--

DROP TABLE IF EXISTS `orders_detail`;
CREATE TABLE IF NOT EXISTS `orders_detail` (
  `Order_ID` int(255) NOT NULL,
  `Product_ID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Quantity` int(11) NOT NULL,
  `Discount` float DEFAULT NULL,
  `Price` float NOT NULL,
  `total` int(200) NOT NULL,
  PRIMARY KEY (`Order_ID`,`Product_ID`),
  KEY `Product_ID` (`Product_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders_detail`
--

INSERT INTO `orders_detail` (`Order_ID`, `Product_ID`, `Quantity`, `Discount`, `Price`, `total`) VALUES
(1, 'Sp1', 11, NULL, 40000, 440000),
(2, 'Sp6', 1, NULL, 22000, 22000),
(5, 'Sp9', 1, NULL, 100000, 100000),
(7, 'Sp9', 2, NULL, 100000, 200000),
(8, 'Sp8', 1, NULL, 22000, 22000),
(9, 'Sp8', 1, NULL, 22000, 22000),
(10, 'Sp8', 1, NULL, 22000, 22000),
(11, 'Sp8', 999, NULL, 22000, 21978000),
(298, 'Sp10', 1, NULL, 120000, 120000),
(299, 'Sp1', 1, NULL, 40000, 40000),
(299, 'Sp10', 1, NULL, 120000, 120000),
(299, 'Sp11', 1, NULL, 59000, 59000),
(378, 'Sp1', 12, NULL, 40000, 480000),
(378, 'Sp10', 3, NULL, 120000, 360000),
(378, 'Sp11', 123, NULL, 59000, 7257000),
(379, 'Sp10', 1, NULL, 120000, 120000),
(379, 'Sp11', 1, NULL, 59000, 59000),
(380, 'Sp11', 2, NULL, 59000, 118000),
(380, 'Sp12', 3, NULL, 115000, 345000),
(401, 'Sp10', 1, NULL, 120000, 120000),
(402, 'Sp1', 1, NULL, 40000, 40000),
(402, 'Sp10', 6, NULL, 120000, 720000),
(402, 'Sp12', 2, NULL, 115000, 230000),
(402, 'Sp13', 12, NULL, 125000, 1500000),
(402, 'Sp15', 1, NULL, 123123, 123123),
(404, 'Sp10', 154, NULL, 120000, 18480000),
(404, 'Sp15', 2, NULL, 123123, 246246),
(405, 'Sp10', 100, NULL, 120000, 12000000),
(405, 'Sp11', 10, NULL, 59000, 590000),
(406, 'Sp1', 1, NULL, 42000, 42000),
(406, 'Sp10', 1, NULL, 120000, 120000),
(406, 'Sp11', 1, NULL, 59000, 59000),
(407, 'Sp10', 2, NULL, 120000, 240000),
(407, 'Sp11', 4, NULL, 59000, 236000),
(408, 'Sp1', 5, NULL, 42000, 210000),
(408, 'Sp10', 1, NULL, 120000, 120000),
(408, 'Sp11', 324, NULL, 59000, 19116000),
(409, 'Sp11', 17, NULL, 59000, 1003000),
(410, 'Sp11', 4, NULL, 59000, 236000);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `Product_ID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Product_Name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `Actual_Price` float NOT NULL,
  `Quoted_Price` float NOT NULL,
  `Category_ID` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Supplier_ID` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Product_ID`),
  KEY `Category_ID` (`Category_ID`),
  KEY `products_ibfk_2` (`Supplier_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`Product_ID`, `Product_Name`, `Actual_Price`, `Quoted_Price`, `Category_ID`, `Supplier_ID`, `img`) VALUES
('Sp1', 'Kẹo Gum Vị Bạc Hà The', 42000, 44000, 'TL2', 'NCC1', 'keo-gum-khong-duong-so-5-vi-bac-ha-the-xanh-duong-1511235009.jpg'),
('Sp10', 'Kẹo Gum Trái Cây Lotte', 120000, 150000, 'TL1', 'NCC1', 'gum-trai-cay-lotte-fruity-assorted-143g-1591171941.jpg'),
('Sp11', 'Kẹo Gum Big Jackpot', 59000, 63000, 'TL1', 'NCC2', 'keo-gum-hello-4_1551062654.jpg'),
('Sp12', 'Gum Hubba Bubba', 115000, 132000, 'TL2', 'NCC2', 'gum-hubba-bubba-thanh-vi-nho-1543744573.jpg'),
('Sp13', 'Kẹo Gum Yoyo Mania', 125000, 135000, 'TL1', 'NCC3', 'keo-singgum-yoyo-mania-2_1543632900.jpg'),
('Sp14', 'Kẹo Gum Trident ICE', 40000, 44000, 'TL1', 'NCC3', 'keo-gum-trident-ice-huong-bac-ha-vi-112g-202007201600476207.jpg'),
('Sp15', 'Sản phẩm 15', 123123, 123343, 'TL1', 'NCC1', 'keo-gum-bong-bong-lotte-fusen-nomi-huong-cola-hu-15g-201909161408517618.jpg'),
('Sp2', 'Kẹo Gum Không Đường Ice Cubes', 120000, 130000, 'TL1', 'NCC1', 'keo-gum-nhai-khong-duong-ice-breakers-ice-cubes-spearmint-peppermint-4_1539944236.jpg'),
('Sp3', 'Kẹo Gum Galaxy Rocks', 58000, 64000, 'TL1', 'NCC2', 'keo-gum-galaxy-rocks-2_1551062653.jpg'),
('Sp4', 'Kẹo Gum Trident Bạc Hà', 22000, 25000, 'TL2', 'NCC2', 'keo-gum-trident-bac-ha-1510372923.jpg'),
('Sp5', 'Kẹo Gum Trident Bạc Hà Quế', 22000, 25000, 'TL2', 'NCC1', 'keo-gum-trident-bac-ha-que-1519976154.jpg'),
('Sp6', 'Kẹo Gum Trident Chanh Dâu', 22000, 25000, 'TL2', 'NCC3', 'keo-gum-trident-chanh-dau-1510728119.jpg'),
('Sp7', 'Kẹo Gum Trident Hương Quế', 22000, 25000, 'TL2', 'NCC2', 'keo-gum-trident-huong-que-1510372984.jpg'),
('Sp8', 'Kẹo Gum Trident Vị Cam Kiwi', 22000, 25000, 'TL2', 'NCC2', 'keo-gum-trident-vi-cam-kiwi-1510372970.jpg'),
('Sp888', 'Kẹo Gum Trái Cây Lotte', 120000, 150000, 'TL1', 'ncc1', 'mieng-ngam-thom-mieng-listerine-1512706991.jpg'),
('Sp9', 'Gum số 5 Peppermint Cobalt 35 stick', 100000, 110000, 'TL2', 'NCC1', 'gum-so-5-peppermint-cobalt-35-stick-1582345548.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `shippers`
--

DROP TABLE IF EXISTS `shippers`;
CREATE TABLE IF NOT EXISTS `shippers` (
  `Shipper_ID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Shipper_Name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Phone_Number` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Shipper_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shippers`
--

INSERT INTO `shippers` (`Shipper_ID`, `Shipper_Name`, `Phone_Number`) VALUES
('Ship1', 'Đặng Văn A', '0123456789'),
('Ship3', 'Hoàng Thị C', '2345678901'),
('Ship4', 'Đào Văn D', '3456789012'),
('Ship5', 'Trần Đình F', '4567890123'),
('Ship6', 'Lê Văn G', '5678901234'),
('Ship7', 'Shiper7', '09809888');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE IF NOT EXISTS `suppliers` (
  `Supplier_ID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Supplier_Name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Supplier_Address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Supplier_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`Supplier_ID`, `Supplier_Name`, `Supplier_Address`) VALUES
('NCC1', 'Công ty TNHH ABC', 'Califonia, Mỹ'),
('NCC2', 'Công ty TNHH XYZ', 'Paris, Pháp'),
('NCC3', 'Nhà cung cấp 3', 'Hà Nam');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`Cus_ID`) REFERENCES `customers` (`Cus_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`Shipper_ID`) REFERENCES `shippers` (`Shipper_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders_detail`
--
ALTER TABLE `orders_detail`
  ADD CONSTRAINT `orders_detail_ibfk_2` FOREIGN KEY (`Product_ID`) REFERENCES `products` (`Product_ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_detail_ibfk_3` FOREIGN KEY (`Order_ID`) REFERENCES `orders` (`Order_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`Category_ID`) REFERENCES `categories` (`Category_ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`Supplier_ID`) REFERENCES `suppliers` (`Supplier_ID`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
